import tensorflow as tf
import os

class TPU:
    def __init__(self):
        self.getStrategy()
        self.settings()

    def set_cwd(self, wd):
        abspath = os.path.abspath(wd)
        dname = os.path.dirname(abspath)
        os.chdir(dname)

    def settings(self):
        if (self.flag == True):
            working = "/kaggle/working"
        else:
            working = "./logs"
        self.working = working

    def getStrategy(self):
        # ############### Setting up the TPUs ###############
        try:
            tpu = tf.distribute.cluster_resolver.TPUClusterResolver()
            tf.config.experimental_connect_to_cluster(tpu)
            tf.tpu.experimental.initialize_tpu_system(tpu)
            strategy = tf.distribute.experimental.TPUStrategy(tpu)
            print("Running on TPU:", tpu.master())
            flag = True
        except ValueError:
            strategy = tf.distribute.get_strategy()
            flag = False
        print(f"Running on {strategy.num_replicas_in_sync} replicas")
        
        self.strategy = strategy
        self.num_workers = 2 * strategy.num_replicas_in_sync
        self.flag = flag