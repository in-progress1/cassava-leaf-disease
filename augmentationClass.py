import tensorflow as tf
import numpy as np

HEIGHT, WIDTH = 512, 512

class Augmentation:
    def __init__(self, batch_size, num_classes, image_size):
        self.batch_size = batch_size
        self.num_classes = num_classes
        self.image_size = image_size

    def augment(self, image, label):
        p_cutout = tf.random.uniform([], 0, 1.0, dtype=tf.float32)

        # Flips
        image = tf.image.random_flip_left_right(image)
        image = tf.image.random_flip_up_down(image)
        # Cutout
        if p_cutout > .5:
            image = self.data_augment_cutout(image)

        return image, label

    def data_augment_cutout(self, image, min_mask_size=(int(HEIGHT * .1), int(HEIGHT * .1)), 
                            max_mask_size=(int(HEIGHT * .125), int(HEIGHT * .125))):
        p_cutout = tf.random.uniform([], 0, 1.0, dtype=tf.float32)

        if p_cutout > .85: # 10~15 cut outs
            n_cutout = tf.random.uniform([], 10, 15, dtype=tf.int32)
            image = self.random_cutout(image, HEIGHT, WIDTH, 
                                  min_mask_size=min_mask_size, max_mask_size=max_mask_size, k=n_cutout)
        elif p_cutout > .6: # 5~10 cut outs
            n_cutout = tf.random.uniform([], 5, 10, dtype=tf.int32)
            image = self.random_cutout(image, HEIGHT, WIDTH, 
                                  min_mask_size=min_mask_size, max_mask_size=max_mask_size, k=n_cutout)
        elif p_cutout > .25: # 2~5 cut outs
            n_cutout = tf.random.uniform([], 2, 5, dtype=tf.int32)
            image = self.random_cutout(image, HEIGHT, WIDTH, 
                                  min_mask_size=min_mask_size, max_mask_size=max_mask_size, k=n_cutout)
        else: # 1 cut out
            image = self.random_cutout(image, HEIGHT, WIDTH, 
                                  min_mask_size=min_mask_size, max_mask_size=max_mask_size, k=1)

        return image

    @staticmethod
    def random_cutout(image, height, width, channels=3, min_mask_size=(10, 10), max_mask_size=(80, 80), k=1):
        assert height > min_mask_size[0]
        assert width > min_mask_size[1]
        assert height > max_mask_size[0]
        assert width > max_mask_size[1]

        for i in range(k):
          mask_height = tf.random.uniform(shape=[], minval=min_mask_size[0], maxval=max_mask_size[0], dtype=tf.int32)
          mask_width = tf.random.uniform(shape=[], minval=min_mask_size[1], maxval=max_mask_size[1], dtype=tf.int32)

          pad_h = height - mask_height
          pad_top = tf.random.uniform(shape=[], minval=0, maxval=pad_h, dtype=tf.int32)
          pad_bottom = pad_h - pad_top

          pad_w = width - mask_width
          pad_left = tf.random.uniform(shape=[], minval=0, maxval=pad_w, dtype=tf.int32)
          pad_right = pad_w - pad_left

          cutout_area = tf.zeros(shape=[mask_height, mask_width, channels], dtype=tf.uint8)

          cutout_mask = tf.pad([cutout_area], [[0,0],[pad_top, pad_bottom], [pad_left, pad_right], [0,0]], constant_values=1)
          cutout_mask = tf.squeeze(cutout_mask, axis=0)
          image = tf.multiply(tf.cast(image, tf.float32), tf.cast(cutout_mask, tf.float32))

        return image

    def cutmix(self, image, label, PROBABILITY = 1.0):
        # input image - is a batch of images of size [n,dim,dim,3] not a single image of [dim,dim,3]
        # output - a batch of images with cutmix applied
        DIM = self.image_size[0]

        imgs = []; labs = []
        for j in range(self.batch_size):
            # DO CUTMIX WITH PROBABILITY DEFINED ABOVE
            P = tf.cast( tf.random.uniform([],0,1)<=PROBABILITY, tf.int32)
            # CHOOSE RANDOM IMAGE TO CUTMIX WITH
            k = tf.cast( tf.random.uniform([],0,self.batch_size),tf.int32)
            # CHOOSE RANDOM LOCATION
            x = tf.cast( tf.random.uniform([],0,DIM),tf.int32)
            y = tf.cast( tf.random.uniform([],0,DIM),tf.int32)
            b = tf.random.uniform([],0,1) # this is beta dist with alpha=1.0
            WIDTH = tf.cast( DIM * tf.math.sqrt(1-b),tf.int32) * P
            ya = tf.math.maximum(0,y-WIDTH//2)
            yb = tf.math.minimum(DIM,y+WIDTH//2)
            xa = tf.math.maximum(0,x-WIDTH//2)
            xb = tf.math.minimum(DIM,x+WIDTH//2)
            # MAKE CUTMIX IMAGE
            one = image[j,ya:yb,0:xa,:]
            two = image[k,ya:yb,xa:xb,:]
            three = image[j,ya:yb,xb:DIM,:]
            middle = tf.concat([one,two,three],axis=1)
            img = tf.concat([image[j,0:ya,:,:],middle,image[j,yb:DIM,:,:]],axis=0)
            imgs.append(img)
            # MAKE CUTMIX LABEL
            a = tf.cast(WIDTH*WIDTH/DIM/DIM,tf.float32)
            if len(label.shape)==1:
                lab1 = tf.one_hot(label[j],self.num_classes)
                lab2 = tf.one_hot(label[k],self.num_classes)
            else:
                lab1 = label[j,]
                lab2 = label[k,]
            labs.append((1-a)*lab1 + a*lab2)

        # RESHAPE HACK SO TPU COMPILER KNOWS SHAPE OF OUTPUT TENSOR (maybe use Python typing instead?)
        image2 = tf.reshape(tf.stack(imgs),(self.batch_size,DIM,DIM,3))
        label2 = tf.reshape(tf.stack(labs),(self.batch_size,self.num_classes))
        return image2,label2

    def mixup(self, image, label, PROBABILITY = 1.0):
        # input image - is a batch of images of size [n,dim,dim,3] not a single image of [dim,dim,3]
        # output - a batch of images with mixup applied
        DIM = self.image_size[0]

        imgs = []; labs = []
        for j in range(self.batch_size):
            # DO MIXUP WITH PROBABILITY DEFINED ABOVE
            P = tf.cast( tf.random.uniform([],0,1)<=PROBABILITY, tf.float32)
            # CHOOSE RANDOM
            k = tf.cast( tf.random.uniform([],0,self.batch_size),tf.int32)
            a = tf.random.uniform([],0,1)*P # this is beta dist with alpha=1.0
            # MAKE MIXUP IMAGE
            img1 = image[j,]
            img2 = image[k,]
            imgs.append((1-a)*img1 + a*img2)
            # MAKE CUTMIX LABEL
            if len(label.shape)==1:
                lab1 = tf.one_hot(label[j],self.num_classes)
                lab2 = tf.one_hot(label[k],self.num_classes)
            else:
                lab1 = label[j,]
                lab2 = label[k,]
            labs.append((1-a)*lab1 + a*lab2)

        # RESHAPE HACK SO TPU COMPILER KNOWS SHAPE OF OUTPUT TENSOR (maybe use Python typing instead?)
        image2 = tf.reshape(tf.stack(imgs),(self.batch_size,DIM,DIM,3))
        label2 = tf.reshape(tf.stack(labs),(self.batch_size,self.num_classes))
        return image2,label2

    def transform(self, image, label):
        # THIS FUNCTION APPLIES BOTH CUTMIX AND MIXUP
        DIM = self.image_size[0]

        SWITCH = 0.4
        CUTMIX_PROB = 0.7
        MIXUP_PROB = 0.7
        # FOR SWITCH PERCENT OF TIME WE DO CUTMIX AND (1-SWITCH) WE DO MIXUP
        image2, label2 = self.cutmix(image, label, CUTMIX_PROB)
        image3, label3 = self.mixup(image, label, MIXUP_PROB)
        imgs = []; labs = []
        for j in range(self.batch_size):
            P = tf.cast( tf.random.uniform([],0,1)<=SWITCH, tf.float32)
            imgs.append(P*image2[j,]+(1-P)*image3[j,])
            labs.append(P*label2[j,]+(1-P)*label3[j,])
        # RESHAPE HACK SO TPU COMPILER KNOWS SHAPE OF OUTPUT TENSOR (maybe use Python typing instead?)
        image4 = tf.reshape(tf.stack(imgs),(self.batch_size,DIM,DIM,3))
        label4 = tf.reshape(tf.stack(labs),(self.batch_size,self.num_classes))
        return image4,label4


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    from matplotlib import image

    datasetPATH = "./dataset/cassava-leaf-disease-classification"

    img_size = (512,512)
    augmentation = Augmentation(4,5,img_size)

    image_names = ["6103","218377","9312065","7288550"]
    images = []
    for image_name in image_names:
        imageArray = image.imread("{0}/train_images/{1}.jpg".format(datasetPATH, image_name))
        imageArray = imageArray/255.0
        imageArray = imageArray[:img_size[0],:img_size[0]]
        images.append(imageArray)

    batch = tf.experimental.numpy.array(images, dtype = tf.float32)
    label = tf.experimental.numpy.array([0,1,2,3])

    #batch = augmentation.transform(batch,label)
    
    aug_image, _ = augmentation.augment(images[0],0)
    plt.imshow(aug_image)#batch[0][0]
    plt.show()