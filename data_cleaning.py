from cleanlab.pruning import get_noise_indices
from datasetClass import ImageDataset
import tensorflow as tf
import efficientnet.tfkeras
import numpy as np
import pandas as pd


#######Fix driver but for now#######
gpu = tf.config.list_physical_devices('GPU')[0]
tf.config.experimental.set_memory_growth(gpu, True)
####################################

def load_model(fold_num):
    dataset.init_fold(fold_num)

    models_dir = "./logs/weights/"
    model = tf.keras.models.load_model(
        models_dir+
        'fold_{0}.h5'.format(fold_num))
    return model, dataset

def get_noisy_predictions(dataset):
    all_predictions = []
    all_labels = []
    all_test_data = []
    for fold_num in range(dataset.n_splits):
        model, dataset = load_model(fold_num)
        model.summary()
        
        predictions = model.predict(dataset.test, verbose = True)
        labels = np.array(dataset.test_data["label"])
        all_predictions.append(predictions)
        all_labels.append(labels)
        all_test_data.append(dataset.test_data)

    all_predictions = np.concatenate(all_predictions)
    all_labels = np.concatenate(all_labels)
    all_test_data = pd.concat(all_test_data)
    return all_predictions, all_labels, all_test_data
    

def get_noisy_images(predictions, labels, test_data):
    ordered_label_errors = get_noise_indices(
        s=labels,
        psx=predictions,
        sorted_index_method='normalized_margin', # Orders label errors
    )
    noisy_data = test_data.iloc[ordered_label_errors]
    noisy_images = noisy_data["image_id"].reset_index(drop=True)
    return noisy_images

dataset = ImageDataset()

all_predictions, all_labels, all_test_data = get_noisy_predictions(dataset)

noisy_csv = get_noisy_images(all_predictions, all_labels, all_test_data)

noisy_csv.columns = ["image_id"]
noisy_csv.to_csv('./logs/csv/noisy_csv.csv', index=False)