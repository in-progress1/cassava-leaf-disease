import tensorflow as tf
import numpy as np
import pandas as pd
from sklearn.model_selection import StratifiedKFold

from extendDatasetClass import ExtendDataset
from augmentationClass import Augmentation

class ImageDataset:
    def __init__(self, num_workers = 1, tpuFlag = False):
        self.datasetName = "cassava-train-512"
        self.num_workers = num_workers
        self.tpuFlag = tpuFlag
        self.extendDataset = ExtendDataset(self.tpuFlag)
        self.datasetPATH = self.extendDataset.get_dataset_path(self.datasetName)
        
        self.batch_size = 4 * self.num_workers
        self.n_splits = 5
        self.num_classes = 5
        self.image_size = (512,512)
        self.soft_P = 0.05

        self.augmentation = Augmentation(self.batch_size,self.num_classes,self.image_size)
        self.seed_fn(2020)
        self.extendDataset.create_soft_labels()
        self.csv_data, self.meta_data = self.read_csv()

    def seed_fn(self,seed):
        self.seed = seed
        tf.random.set_seed(seed)
        np.random.seed(seed)

    def init_fold(self, fold_name, remove_noisy = False):
        if (fold_name == "meta"):
            self.train_data = self.csv_data
            self.test_data = self.meta_data
            self.init_datasets(remove_noisy)
        elif (type(fold_name) is int):
            fold_num = fold_name
            n_splits = self.n_splits
            dataset = self.csv_data
            self.set_fold_number(dataset, fold_num, n_splits, remove_noisy)

    def set_fold_number(self, dataset, fold_num, n_splits, remove_noisy):
        k_fold = StratifiedKFold(n_splits, shuffle=True, random_state=self.seed)
        k_fold = k_fold.split(dataset["image_id"], dataset["label"])
        k_fold = list(k_fold)
        train_index, test_index = k_fold[fold_num]
        self.train_data = dataset.iloc[train_index]
        self.test_data = dataset.iloc[test_index]
        self.init_datasets(remove_noisy)

    def read_csv(self):
        csvPATH = f"{self.datasetPATH}/train.csv"
        csv_data = pd.read_csv(csvPATH)
        csv_data["image_path"] = f"{self.datasetPATH}/train_images/" + csv_data["image_id"]

        csv_data = self.extendDataset.add_soft_labels(csv_data)
        csv_data, meta_data = self.extendDataset.cut_off_meta_data(csv_data, self.seed)
        return csv_data, meta_data

    def init_datasets(self, remove_noisy):
        #self.train_data = self.extendDataset.unify_and_shuffle(self.train_data, self.seed)
        if (remove_noisy == True):
            self.train_data = self.extendDataset.remove_noisy(self.train_data)
            #self.test_data = self.extendDataset.remove_noisy(self.test_data)#Remove noisy labels from train and test set
        #self.train_data = self.extendDataset.oversample(self.train_data, self.seed)
        self.train_data = self.extendDataset.align_image_distribution(self.train_data)

        self.train = self.create_dataset(self.train_data)
        self.test = self.create_dataset(self.test_data, is_training = False)

    def create_dataset(self, data, is_training=True):
        AUTOTUNE = tf.data.experimental.AUTOTUNE
        dataset=tf.data.Dataset.from_tensor_slices((data["image_path"],data["label"],
                                np.stack(data["soft_label"], axis = 0)              ))
        if is_training==True:
            dataset = dataset.shuffle(1000)

        dataset = dataset.map(lambda logit,label,soft_label:
                                self.map_dataset(logit,label,soft_label,is_training),num_parallel_calls = AUTOTUNE)
        if (self.tpuFlag == True):
            dataset = dataset.cache()
        if (is_training == True):
            dataset = dataset.map(self.augmentation.augment,num_parallel_calls = AUTOTUNE)
            # dataset = dataset.batch(self.batch_size, drop_remainder=True)
            # dataset = dataset.map(self.augmentation.transform, num_parallel_calls = AUTOTUNE)
            # dataset = dataset.unbatch()

        if is_training==True:
            dataset = dataset.shuffle(1000)
        dataset=dataset.batch(self.batch_size, drop_remainder = is_training)
        dataset=dataset.prefetch(AUTOTUNE)
        return dataset

    def map_dataset(self,file_path, label, soft_label, is_training):
        image = tf.io.decode_jpeg(tf.io.read_file(file_path), channels = 3)
        image = tf.image.resize(image, size=self.image_size)
        image = image / 255.0
        
        label = tf.one_hot(label,self.num_classes)
        if (is_training == True):
            label = label * self.soft_P + soft_label * (1 - self.soft_P)
        return image, label


if __name__ == "__main__":
    import matplotlib.pyplot as plt

    dataset = ImageDataset()
    dataset.init_fold(0)
    data = next(iter(dataset.train))
    plt.imshow(data[0][0])
    plt.show()