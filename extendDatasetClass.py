import tensorflow as tf
from os import path
import pandas as pd
import numpy as np

from sklearn.model_selection import StratifiedKFold


class ExtendDataset:
    def __init__(self, tpuFlag = False):
        self.labels={
            "cbb":0,
            "cbsd":1,
            "cgm":2,
            "cmd":3,
            "healthy":4
        }
        self.tpuFlag = tpuFlag
        self.datasetName = "cassava-disease-2019"
        self.datasetPATH = self.get_dataset_path(self.datasetName)
        self.train_path = self.datasetPATH + "/train"
        
    def get_dataset_path(self, datasetName):
        if (self.tpuFlag == True):
            GS_bucket = {
                "cassava-leaf-disease-classification":"gs://kds-9254a9aba4b37289f07bbbbe4e2af5c952dd2d811a2c823340ca8091",
                "cassava-disease-2019":"gs://kds-6b9c9b983e9a98d0dbfa26e348ee03d4411d135cf77da563857a53c0",
                "cassava-train-512":"gs://kds-4fcc975355a27ca1ecde8955904cd0e9e7626bcb66024b3778638fb4"
            }
            dataset_path = GS_bucket[datasetName]
        else:
            dataset_path = f"./dataset/{datasetName}"
        return dataset_path

    def read_folder(self,folderPath):
        files = tf.io.gfile.glob(f"{folderPath}/*")
        files = [path.basename(file) for file in files]
        files = sorted(files)
        return files

    def create_dataset(self,class_name):
        folder_path = f"{self.train_path}/{class_name}"
        image_names = self.read_folder(folder_path)
        image_paths = [f"{self.datasetPATH}/train/{class_name}/{image_name}" for image_name in image_names]
        dataset = pd.DataFrame({"image_id":image_names,"image_path":image_paths,"label":self.labels[class_name]})
        return dataset

    def create(self, remove = ["cmd"]):
        data_classes = self.read_folder(self.train_path)
        data_classes = list(set(data_classes)-set(remove))
        dataset_list = []
        for class_name in data_classes:
            class_dataset = self.create_dataset(class_name)
            dataset_list.append(class_dataset)
        dataset = pd.concat(dataset_list)
        dataset = dataset.sort_values(["label","image_id"])
        return dataset

    def unify_and_shuffle(self, main_dataset, seed):
        self.train = self.create()
        main_dataset = pd.concat([main_dataset,self.train])
        main_dataset = main_dataset.sample(frac=1, random_state = seed).reset_index(drop=True)
        return main_dataset

    def oversample(self, dataset, seed, remove = "cmd"):
        filterMask = (dataset["label"] != self.labels[remove])
        oversample_dataset = dataset[filterMask]
        oversample_dataset = pd.concat([dataset,oversample_dataset])
        oversample_dataset = oversample_dataset.sample(frac=1, random_state = seed).reset_index(drop=True)
        return oversample_dataset

    def remove_from_dataset(self, dataset, remove_data):
        remove_condition = dataset['image_id'].isin(remove_data['image_id'])
        dataset = dataset.drop(dataset[remove_condition].index)
        #Remove values of one dataframe from another
        return dataset

    def remove_noisy(self, dataset, noisy_csv = './logs/csv/noisy_csv.csv'):
        noisy_csv = pd.read_csv(noisy_csv)
        part_noisy_csv = noisy_csv.iloc[:727]
        dataset = self.remove_from_dataset(dataset, part_noisy_csv)
        return dataset
        
    def cut_off_meta_data(self, csv_data, seed):
        k_fold = StratifiedKFold(50, shuffle=True, random_state=seed)
        k_fold = k_fold.split(csv_data["image_id"],csv_data["label"])
        k_fold = list(k_fold)
        _, meta_index = k_fold[-1]
        meta_data = csv_data.iloc[meta_index]
        
        csv_data = self.remove_from_dataset(csv_data, meta_data)
        return csv_data, meta_data

    def align_image_distribution(self, dataset):
        k_fold = StratifiedKFold(100)
        k_fold = k_fold.split(dataset["image_id"], dataset["label"])
        k_fold = list(k_fold)
        Folds = []
        for (_, test_index) in k_fold:
            sub_dataset = dataset.iloc[test_index]#drop index?
            Folds.append(sub_dataset)
        aligned_dataset = pd.concat(Folds)
        return aligned_dataset

    def create_soft_labels(self, soft_csv = "./logs/csv/soft_targets_2020.csv"):
        def predictions(row):
            preds_row = row[["p0","p1","p2","p3","p4"]]
            preds = preds_row.to_numpy(dtype = np.float32)
            return preds


        soft_csv = pd.read_csv(soft_csv)
        preds_series = soft_csv.apply(predictions, axis = 1)
        soft_csv["predictions"] = preds_series
        self.soft_csv = soft_csv


    def add_soft_labels(self, csv_data):
        def get_soft_labels(row):
            image_name = row["image_id"]
            image_row = self.soft_csv.loc[self.soft_csv['image_id'] == image_name]
            soft_labels = image_row["predictions"].iloc[0]
            return soft_labels

        soft_labels = csv_data.apply(get_soft_labels, axis = 1)

        csv_data = csv_data.copy()
        csv_data["soft_label"] = soft_labels
        return csv_data


if __name__ == "__main__":
    extendDataset = ExtendDataset()
   
    dataset = extendDataset.unify_and_shuffle(pd.DataFrame(),2021)
    print(extendDataset.train)
    
    #dataset = extendDataset.oversample(dataset, 2021)
    print(dataset)