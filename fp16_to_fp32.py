import tensorflow as tf
import efficientnet.tfkeras
import os


policy = tf.keras.mixed_precision.experimental.Policy('float32')
tf.keras.mixed_precision.experimental.set_policy(policy)


def get_new_name(name):
    fp32_dir = "./logs/weights_fp32/"
    model_name = name.split("/")[-1]
    new_name = fp32_dir+model_name
    return new_name


models_dir = "./logs/weights/"
models = os.listdir(models_dir)
models = [models_dir+model for model in models]


for model_name in models:
    model = tf.keras.models.load_model(model_name, compile=False)
    model.compile(loss='categorical_crossentropy',
                optimizer=tf.keras.optimizers.Adam(),
                metrics=['categorical_accuracy'])

    new_name = get_new_name(model_name)
    model.save(new_name)



