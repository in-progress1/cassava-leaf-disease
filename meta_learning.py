
import tensorflow as tf
import os
from tensorflow.keras.callbacks import ModelCheckpoint
import efficientnet.tfkeras

from datasetClass import ImageDataset



class MetaModel:
    def __init__(self, num_classes):
        self.num_classes = num_classes
        self.load_models()
        self.create()

    def load_models(self, models_dir = "./logs/weights/"):
        model_names = sorted(os.listdir(models_dir))
        models = []
        for ind in range(len(model_names)):
            model_name = model_names[ind]
            model_name = models_dir + model_name
            model = tf.keras.models.load_model(model_name)
            models.append(model)
        
        self.models = models

    def create(self):
        model = tf.keras.models.Sequential()
        model.add(tf.keras.Input((None,None,3)))
        model.add(tf.keras.layers.Lambda(self.EnsembleLayer))
        model.add(tf.keras.layers.Dense(self.num_classes, 
                                    kernel_regularizer = tf.keras.regularizers.L2(0.01)))
        model.add(tf.keras.layers.Activation('softmax', dtype='float32', name='predictions'))
        #model.layers[0].trainable = False

        self.meta_model = model

    def load(self,model_path = "./logs/meta_model.h5"):
        self.meta_model.load_weights(model_path)

    def EnsembleLayer(self, image):
        ensemble = []
        for model in self.models:
            fold = model(image)
            ensemble.append(fold)
        concatenation = tf.keras.layers.Concatenate()(ensemble)
        return concatenation



class MetaLearning:
    def __init__(self):
        self.dataset = ImageDataset()
        self.metaModel = MetaModel(self.dataset.num_classes) 

    def compile(self, fold_num):
        self.dataset.init_fold("meta", remove_noisy = False)
        self.init_callbacks()

        self.metaModel.meta_model.compile(loss='categorical_crossentropy',
            optimizer=tf.keras.optimizers.Adam(lr = 1e-3),
            metrics=['categorical_accuracy'])
        self.metaModel.meta_model.summary()

    def init_callbacks(self):
        model_save = ModelCheckpoint('./logs/meta_model.h5', 
                                     save_best_only = True, 
                                     monitor = 'val_loss', 
                                     verbose = 1)
        self.callbacks = [model_save]

    def train(self, fold_num = 0, epochs = 1):
        self.compile(fold_num)
        self.metaModel.meta_model.fit(x=self.dataset.train, epochs = epochs,
                        validation_data = self.dataset.test,
                        callbacks=self.callbacks)



if __name__ == "__main__":
    #######Fix driver but for now#######
    gpu = tf.config.list_physical_devices('GPU')[0]
    tf.config.experimental.set_memory_growth(gpu, True)
    ####################################

    metaLearning = MetaLearning()
    metaLearning.train(epochs = 5)
