import tensorflow as tf
import efficientnet.tfkeras
import pandas as pd
import numpy as np
import os

#######Fix driver but for now#######
gpu = tf.config.list_physical_devices('GPU')[0]
tf.config.experimental.set_memory_growth(gpu, True)
####################################

class MetaModel:
    def __init__(self, num_classes):
        self.num_classes = num_classes
        self.load_models()
        self.create()

    def load_models(self, models_dir = "./logs/weights/"):
        model_names = sorted(os.listdir(models_dir))
        models = []
        for ind in range(len(model_names)):
            model_name = model_names[ind]
            model_name = models_dir + model_name
            model = tf.keras.models.load_model(model_name)
            models.append(model)
        
        self.models = models

    def create(self):
        model = tf.keras.models.Sequential()
        model.add(tf.keras.Input((None,None,3)))
        model.add(tf.keras.layers.Lambda(self.EnsembleLayer))
        model.add(tf.keras.layers.Dense(self.num_classes))
        model.add(tf.keras.layers.Activation('softmax', dtype='float32', name='predictions'))
        #model.layers[0].trainable = False

        self.meta_model = model

    def load(self,model_path = "./logs/meta_model.h5"):
        self.meta_model.load_weights(model_path)

    def EnsembleLayer(self, image):
        ensemble = []
        for model in self.models:
            fold = model(image)
            ensemble.append(fold)
        concatenation = tf.keras.layers.Concatenate()(ensemble)
        return concatenation

def create_dataset(data):
    AUTOTUNE = tf.data.experimental.AUTOTUNE
    dataset=tf.data.Dataset.from_tensor_slices((data["image_id"],data["label"]))
    dataset = dataset.map(lambda logit,label:map_dataset(logit,label),num_parallel_calls = AUTOTUNE)
    
    dataset=dataset.batch(BATCH_SIZE)
    dataset=dataset.prefetch(AUTOTUNE)
    return dataset

def map_dataset(file_name,label):
        file_path = tf.strings.join([data_dir+"test_images/",file_name])
        image = tf.io.decode_jpeg(tf.io.read_file(file_path), channels = 3)
        image = tf.image.resize(image, size=IMAGE_SIZE)
        image = image / 255.0

        label = tf.one_hot(label,NUM_CLASSES)
        return image, label

BATCH_SIZE = 16
IMAGE_SIZE = (512,512)
NUM_CLASSES = 5

data_dir = "./dataset/cassava-leaf-disease-classification/"  
sub_df = pd.read_csv(data_dir+"sample_submission.csv")
test_dataset = create_dataset(sub_df)



metaModel = MetaModel(NUM_CLASSES)
metaModel.load()

predictions = metaModel.meta_model.predict(test_dataset, verbose=1)
print(predictions)


sub_df['label'] = predictions.argmax(axis=1)
sub_df.to_csv('./logs/submission.csv', index=False)