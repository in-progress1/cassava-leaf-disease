from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from datasetClass import ImageDataset
import tensorflow as tf
import efficientnet.tfkeras
import numpy as np
import os


#######Fix driver but for now#######
gpu = tf.config.list_physical_devices('GPU')[0]
tf.config.experimental.set_memory_growth(gpu, True)
####################################

class UnifiedModel:
    def __init__(self, num_classes):
        self.num_classes = num_classes
        self.load_models()
        self.create()

    def load_models(self, models_dir = "./logs/weights/"):
        model_names = sorted(os.listdir(models_dir))
        models = []
        for ind in range(len(model_names)):
            model_name = model_names[ind]
            model_name = models_dir + model_name
            model = tf.keras.models.load_model(model_name)
            models.append(model)
        
        self.models = models

    def create(self):
        model = tf.keras.models.Sequential()
        model.add(tf.keras.Input((None,None,3)))
        model.add(tf.keras.layers.Lambda(self.UnifiedLayer))
        #model.layers[0].trainable = False

        model.summary()
        self.unified_model = model

    def UnifiedLayer(self, image):
        ensemble = []
        for model in self.models:
            fold = model(image)
            softmax = tf.keras.layers.Softmax()(fold)
            ensemble.append(softmax)

        unified = tf.keras.layers.Add()(ensemble)
        return unified


def report_accuracy():
    predictions = unifiedModel.unified_model.predict(dataset.test, verbose = True)
    prediction_labels = np.argmax(predictions, axis=-1)
    valid_labels = np.array(dataset.test_data["label"])
    print(f"Fold - {fold_num}")
    print("Accuracy:",accuracy_score(valid_labels, prediction_labels))
    print(classification_report(valid_labels, prediction_labels))
    print(confusion_matrix(valid_labels, prediction_labels))
    plot_confidence(predictions, prediction_labels, valid_labels)

def plot_confidence(predictions, prediction_labels, valid_labels):
    import scipy
    import matplotlib.pyplot as plt

    predictions = predictions[prediction_labels == valid_labels]
    #predictions = scipy.special.softmax(predictions, axis = 1)
    predictions = np.sort(predictions, axis = 1)

    predictions = predictions / 5

    confidence = lambda x: len(predictions[(predictions[:,4] - predictions[:,3]) > x])/len(prediction_labels)
    alpha = np.linspace(0,0.05,100)
    conf = [confidence(alph) for alph in alpha]
    plt.plot(alpha, conf)
    plt.plot(alpha, [confidence(0.03)] * len(alpha))
    print(f"Confidence 0.03 = {confidence(0.03)}")
    plt.show()

fold_num = "meta"

dataset = ImageDataset()
dataset.init_fold(fold_num)

    

unifiedModel = UnifiedModel(num_classes = 5)


report_accuracy()