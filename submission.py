import tensorflow as tf
import efficientnet.tfkeras
import pandas as pd
import numpy as np
import os

#######Fix driver but for now#######
gpu = tf.config.list_physical_devices('GPU')[0]
tf.config.experimental.set_memory_growth(gpu, True)
####################################


class UnifiedModel:
    def __init__(self, num_classes):
        self.num_classes = num_classes
        self.load_models()
        self.create()

    def load_models(self, models_dir = "./logs/weights/"):
        model_names = sorted(os.listdir(models_dir))
        models = []
        for ind in range(len(model_names)):
            model_name = model_names[ind]
            model_name = models_dir + model_name
            model = tf.keras.models.load_model(model_name)
            models.append(model)
        
        self.models = models

    def create(self):
        model = tf.keras.models.Sequential()
        model.add(tf.keras.Input((None,None,3)))
        model.add(tf.keras.layers.Lambda(self.UnifiedLayer))
        #model.layers[0].trainable = False

        model.summary()
        self.unified_model = model

    def UnifiedLayer(self, image):
        ensemble = []
        for model in self.models:
            fold = model(image)
            softmax = tf.keras.layers.Softmax()(fold)
            ensemble.append(softmax)

        unified = tf.keras.layers.Add()(ensemble)
        return unified


##################################################################

def augment(image, label):
    p_spatial = tf.random.uniform([], 0, 1.0, dtype = tf.float32)
    p_rotate = tf.random.uniform([], 0, 1.0, dtype = tf.float32)
    p_pixel_1 = tf.random.uniform([], 0, 1.0, dtype = tf.float32)
    p_pixel_2 = tf.random.uniform([], 0, 1.0, dtype = tf.float32)
    p_pixel_3 = tf.random.uniform([], 0, 1.0, dtype = tf.float32)

    # Flips
    image = tf.image.random_flip_left_right(image)
    image = tf.image.random_flip_up_down(image)
    if p_spatial > 0.75:
        image = tf.image.transpose(image)

    # Rotates
    if p_rotate > 0.75:
        image = tf.image.rot90(image, k = 3) # rotate 270º
    elif p_rotate > 0.5:
        image = tf.image.rot90(image, k = 2) # rotate 180º
    elif p_rotate > 0.25:
        image = tf.image.rot90(image, k = 1) # rotate 90º

    # Pixel-level transforms
    if p_pixel_1 >= 0.4:
        image = tf.image.random_saturation(image, lower = 0.7, upper = 1.3)
    if p_pixel_2 >= 0.4:
        image = tf.image.random_contrast(image, lower = 0.8, upper = 1.2)
    if p_pixel_3 >= 0.4:
        image = tf.image.random_brightness(image, max_delta = 0.1)

    return image, label

##################################################################

def create_dataset(data):
    AUTOTUNE = tf.data.experimental.AUTOTUNE
    dataset=tf.data.Dataset.from_tensor_slices((data["image_id"],data["label"]))
    dataset = dataset.map(lambda logit,label:map_dataset(logit,label),num_parallel_calls = AUTOTUNE)
    
    dataset = dataset.map(augment,num_parallel_calls = AUTOTUNE)
    
    dataset=dataset.batch(BATCH_SIZE)
    dataset=dataset.prefetch(AUTOTUNE)
    return dataset

def map_dataset(file_name,label):
    file_path = tf.strings.join([data_dir+"test_images/",file_name])
    image = tf.io.decode_jpeg(tf.io.read_file(file_path), channels = 3)
    image = tf.image.resize(image, size=IMAGE_SIZE)
    image = image / 255.0
        
    return image, label

##################################################################

TTA = 5
BATCH_SIZE = 16
IMAGE_SIZE = (512,512)
NUM_CLASSES = 5
data_dir = "./dataset/cassava-leaf-disease-classification/"  
sub_df = pd.read_csv(data_dir+"sample_submission.csv")
test_dataset = create_dataset(sub_df)



##################################################################

unifiedModel = UnifiedModel(NUM_CLASSES)

##################################################################

import scipy
predictions = 0
for step in range(TTA):
    print(f"TTA step {step}")
    preds = unifiedModel.unified_model.predict(test_dataset, verbose=1)
    print(preds)
    predictions = np.add(predictions, preds)

##################################################################

print(predictions)

##################################################################

sub_df['label'] = predictions.argmax(axis=1)
sub_df.to_csv('./logs/submission.csv', index=False)
print(sub_df)