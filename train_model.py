import tensorflow as tf
from tensorflow.keras.callbacks import ModelCheckpoint
import efficientnet.tfkeras as efficientnet
from vit_keras import vit

from biTemperedLogisticLossClass import BiTemperedLogisticLoss
from datasetClass import ImageDataset
from TPUclass import TPU


class NeuralNetwork():
    def __init__(self):
        self.model_type = "B3_ns"
        self.tpu = TPU()
        self.tpu.set_cwd(__file__)
        self.dataset = ImageDataset(self.tpu.num_workers, self.tpu.flag)
        with self.tpu.strategy.scope():
            self.create()

    def compile(self, fold_num = 0, epochs = 10):
        self.dataset.init_fold(fold_num, remove_noisy = False)
        self.init_callbacks(fold_num)

        self.base_model.load_weights(f"./logs/models/{self.model_type}.h5")
        self.model.compile(loss=BiTemperedLogisticLoss(t1 = 0.9, t2 = 1.05),
            optimizer=tf.keras.optimizers.Adam(),
            metrics=['categorical_accuracy'])
        self.model.summary()

    def mixed_precision(self):
        policy = tf.keras.mixed_precision.experimental.Policy('mixed_float16', loss_scale=128)
        tf.keras.mixed_precision.experimental.set_policy(policy)

    def create(self, mixed_precision = True):
        if (mixed_precision == True) and (self.tpu.flag != True):
            tf.config.optimizer.set_jit(True)
            self.mixed_precision()
        if (self.model_type == "B3_ns"):
            base_model = efficientnet.EfficientNetB3(include_top = False,
                                    weights = 'noisy-student')#Can eat images of any size
        elif (self.model_type == "vit_b16"):
            base_model = vit.vit_b16(image_size = self.dataset.image_size[0],
                                    include_top = False, pretrained_top = False)

        model = tf.keras.models.Sequential()
        model.add(base_model)
        if (self.model_type == "B3_ns"):
            model.add(tf.keras.layers.GlobalAveragePooling2D())
        model.add(tf.keras.layers.Dropout(0.2))
        model.add(tf.keras.layers.Dense(self.dataset.num_classes)) #Next autodetect
        model.add(tf.keras.layers.Activation('linear', dtype='float32', name='predictions'))
        
        self.base_model = base_model
        self.model = model

    def init_callbacks(self, fold_num):
        lr_factor = 1 if (self.tpu.flag == False) \
                      else self.tpu.num_workers

        def get_lr_callback():
            lr_start   = 1e-6
            lr_max     = 1.2e-5 * lr_factor
            lr_min     = 1e-6
            lr_ramp_ep = 1
            lr_sus_ep  = 0
            lr_decay   = 0.8

            def lrfn(epoch):
                if epoch < lr_ramp_ep:
                    lr = (lr_max - lr_start) / lr_ramp_ep * epoch + lr_start   
                elif epoch < lr_ramp_ep + lr_sus_ep:
                    lr = lr_max    
                else:
                    lr = (lr_max - lr_min) * lr_decay**(epoch - lr_ramp_ep - lr_sus_ep) + lr_min    
                return lr
            
            lr_callback = tf.keras.callbacks.LearningRateScheduler(lrfn, verbose = True)
            return lr_callback

        model_save = ModelCheckpoint(f'{self.tpu.working}/weights/fold_{fold_num}.h5', 
                                     save_best_only = True, 
                                     monitor = 'val_categorical_accuracy', 
                                     verbose = 1)

        self.callbacks = [get_lr_callback(), model_save]

    def train(self,*train_params):#Can extend change of training parameters
        for params in train_params:
            fold_num, epochs = params
            self.train_step(fold_num, epochs)

    def train_step(self, fold_num, epochs):
        with self.tpu.strategy.scope():
            self.compile(fold_num, epochs)
        self.model.fit(x=self.dataset.train, epochs = epochs,
                        validation_data = self.dataset.test,
                        callbacks=self.callbacks)

if __name__ == "__main__":
    #######Fix driver but for now#######
    try:
        gpu = tf.config.list_physical_devices('GPU')[0]
        tf.config.experimental.set_memory_growth(gpu, True)
    except IndexError:
        pass
    ####################################

    neuralNetwork = NeuralNetwork()
    neuralNetwork.train([0,20],[1,20],[2,20],[3,20],[4,20])#Bad, but OK