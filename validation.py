from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from datasetClass import ImageDataset
import tensorflow as tf
import efficientnet.tfkeras
import numpy as np


#######Fix driver but for now#######
gpu = tf.config.list_physical_devices('GPU')[0]
tf.config.experimental.set_memory_growth(gpu, True)
####################################

def report_accuracy():
    predictions = model.predict(dataset.test, verbose = True)
    prediction_labels = np.argmax(predictions, axis=-1)
    valid_labels = np.array(dataset.test_data["label"])
    
    print(f"Fold - {fold_num}")
    print("Accuracy:",accuracy_score(valid_labels, prediction_labels))
    print(classification_report(valid_labels, prediction_labels))
    print(confusion_matrix(valid_labels, prediction_labels))
    plot_confidence(predictions, prediction_labels, valid_labels)
    
def plot_confidence(predictions, prediction_labels, valid_labels):
    import scipy
    import matplotlib.pyplot as plt

    predictions = predictions[prediction_labels == valid_labels]
    predictions = scipy.special.softmax(predictions, axis = 1)
    predictions = np.sort(predictions, axis = 1)

    confidence = lambda x: len(predictions[(predictions[:,4] - predictions[:,3]) > x])/len(prediction_labels)
    alpha = np.linspace(0,0.05,100)
    conf = [confidence(alph) for alph in alpha]
    plt.plot(alpha, conf)
    plt.plot(alpha, [confidence(0.03)] * len(alpha))
    print(f"Confidence 0.03 = {confidence(0.03)}")
    plt.show()



def load_model(fold_num):
    dataset = ImageDataset()
    dataset.init_fold(fold_num)

    models_dir = "./logs/weights/"
    model = tf.keras.models.load_model(
        models_dir+
        'fold_{0}.h5'.format(fold_num)
    )
    return model, dataset
    

fold_num = 0

model, dataset = load_model(fold_num)
model.summary()


report_accuracy()